Steps to initialize the project : 

1. Git clone.
2. composer install..
3. php init -> choose 0 - Development
4. edit the db section in /common/config/common-local.php to match your credentials
5. php yii migrate/up:
6. php yii rbac/init:
7. go to frontend, register -> that's all
8. If you want to check the backend, change the role of any user manually in db to 'admin', then you'll be able to login in admin.