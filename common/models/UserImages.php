<?php
namespace common\models;

use dezmont765\yii2bundle\behaviors\FileSaveBehavior;
use dezmont765\yii2bundle\models\MainActiveRecord;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "user_images".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 *
 * @property User $user
 * @mixin FileSaveBehavior
 */
class UserImages extends MainActiveRecord
{
    const TYPE_DEFAULT = 'default';


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_images';
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
             'targetAttribute' => ['user_id' => 'id']],
        ];
    }




    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
