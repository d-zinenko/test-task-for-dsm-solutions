<?php
use dezmont765\yii2bundle\views\MainView;
use frontend\filters\SiteLayout;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this MainView */
$this->beginContent('@frontend/views/layouts/base-layout.php'); ?>
<?php
NavBar::begin([
                  'brandLabel' => Yii::t('app', 'Frontend panel'),
                  'brandUrl' => Yii::$app->homeUrl,
                  'options' => [
                      'class' => 'navbar-default navbar-fixed-top',
                  ],
              ]);
echo Nav::widget([
                     'options' => ['class' => 'navbar-nav navbar-left'],
                     'items' => $this->getLayoutData(SiteLayout::place_left_nav)
                 ]);
echo Nav::widget([
                     'options' => ['class' => 'navbar-nav navbar-right'],
                     'items' => $this->getLayoutData(SiteLayout::place_right_nav)
                 ]);
NavBar::end();
?>
<div class="container">
    <div class="row">
        <?= \dezmont765\yii2bundle\components\Alert::printAlert() ?>
    </div>

    <?= $content ?>
</div>
<? $this->endContent() ?>

