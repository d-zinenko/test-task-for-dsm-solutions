<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <br>
    <?php $form = ActiveForm::begin([
                                        'id' => 'image-upload-form',
                                        'enableAjaxValidation' => false,
                                        'enableClientValidation' => true,
                                        'options' => ['enctype' => 'multipart/form-data']
                                    ]); ?>
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>true]); ?>


    <div class="form-group">
        <?= Html::submitButton('Upload picture',['class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
