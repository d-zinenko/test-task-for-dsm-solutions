<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('user', 'List users');
?>
<div class="user-index">
    <br>
    <?= GridView::widget([
                             'id' => 'picture-list',
                             'dataProvider' => $dataProvider,
                             'filterModel' => $searchModel,
                             'columns' => [
                                 [
                                     'attribute' => 'name',
                                     'format' => 'raw',
                                     'value' => function (\common\models\UserImages $data) {
                                         return Html::img(\common\models\User::getLogged(true)
                                                                             ->getFileByRelationId($data->id, 'picture',
                                                                                                   false, true),
                                                          ['width' => 100]);
                                     }
                                 ],
                                 ['class' => 'yii\grid\ActionColumn',
                                  'template' => '{rotate}{delete}',
                                  'buttons' => [
                                      'rotate' => function ($url, $model, $key) {
                                          $url = Url::to(['user/rotate-picture', 'id' => $key]);
                                          return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                              'title' => Yii::t('yii', 'Rotate'),
                                          ]);
                                      },
                                      'delete' => function ($url, $model, $key) {
                                          $url = Url::to(['user/delete-picture', 'id' => $key]);
                                          return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                              'title' => Yii::t('yii', 'Delete'),
                                              'data-confirm' => 'Are you sure you want to delete this picture?',
                                              'data-method' => 'post',
                                              'data-pjax' => '0'
                                          ]);
                                      }
                                  ],
                                 ],
                             ],
                         ]); ?>

</div>

