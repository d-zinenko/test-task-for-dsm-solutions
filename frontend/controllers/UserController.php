<?php
/**
 * Created by PhpStorm.
 * User: DezMonT
 * Date: 01.04.2015
 * Time: 18:58
 */
namespace frontend\controllers;

use common\models\User;
use common\models\UserImages;
use common\models\UserImagesSearch;
use console\controllers\RbacController;
use dezmont765\yii2bundle\components\Alert;
use dezmont765\yii2bundle\controllers\MainController;
use frontend\filters\UserLayout;
use Yii;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class UserController extends MainController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'layout' => UserLayout::className()
        ];
    }


    /**
     * Displays a single User model.
     * @return mixed
     */
    public function actionView() {
        $model = $this->findModel(User::className(), Yii::$app->user->id);
        self::checkAccess(RbacController::update_profile, ['model' => $model]);
        return $this->render('user-view', [
            'model' => $model,
        ]);
    }


    /**
     * @throws HttpException
     * check users by received code, and if everything is ok, marks them as verified
     */
    public function actionVerifyEmail() {
        $code = Yii::$app->request->getQueryParam('code');
        if($code) {
            $user = User::findOne(['email_verification_code' => $code]);
            if(!($user instanceof User)) {
                throw new NotFoundHttpException(Yii::t('messages', 'Invalid code'));
            }
            $user->email_verification_status = User::EMAIL_VERIFIED;
            if(!$user->save()) {
                throw new ServerErrorHttpException(Yii::t('messages',
                                                          'Something goes wrong. Please contact us, or try again later.'));
            }
            $user->sendWelcomeMail();
            return $this->goHome();
        }
        else {
            throw new BadRequestHttpException('Invalid code');
        }
    }


    public function actionGetVerificationMail() {
        /**@var User $user */
        $user = Yii::$app->user->identity;
        if($user->renewVerificationCode()) {
            $user->sendVerificationEmail($user->email_verification_code);
            return $this->redirect(['user/view', 'id' => $user->id]);
        }
        else {
            return $this->goHome();
        }
    }


    public function actionUploadPicture() {
        $model = User::getLogged(true);
        if($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        return $this->render('upload-picture-form', ['model' => $model]);
    }


    public function actionPictures() {
        $model = new UserImagesSearch();
        $model->load(Yii::$app->request->queryParams);
        $dataProvider = $model->search(UserImages::find()->joinWith(['user' => function (ActiveQuery $query) {
            $query->andWhere(['user.id' => Yii::$app->user->id]);
        }]));
        return $this->render('pictures-list', ['dataProvider' => $dataProvider, 'searchModel' => $model]);
    }


    public function actionRotatePicture($id) {
        $user = User::getLogged(true);
        $user->rotateImage($id);
        return $this->redirect(['pictures']);
    }


    public function actionDeletePicture($id) {
        $user = User::getLogged(true);
        if($user->deleteFileByRelationId('picture', $id)) {
            Alert::addSuccess('Picture has been deleted');
        }
        return $this->redirect(['pictures']);
    }

}