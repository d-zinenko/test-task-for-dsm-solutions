<?php
namespace frontend\filters;

use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: DezMonT
 * Date: 01.04.2015
 * Time: 19:10
 * @method static upload_pictures()
 * @method static pictures()
 */
class UserLayout extends TabbedLayout
{
    public static function layout(array $active = []) {
        $active = array_merge($active, [SiteLayout::profile()]);
        $nav_bar = parent::layout($active);
        return $nav_bar;
    }


    public static function getActiveMap() {
        return [
            'upload-picture' => [UserLayout::upload_pictures()],
            'view' => [UserLayout::profile()],
            'pictures' => [UserLayout::pictures()]
        ];
    }


    public static function getTabs(array $active = []) {
        $tabs = [
            ['label' => 'My profile',
             'url' => Url::to(['user/view']),
             'active' => self::getActive($active, UserLayout::profile())
            ],
            ['label' => 'Upload picture', 'url' => Url::to(['user/upload-picture']),
             'active' => self::getActive($active, UserLayout::upload_pictures())],
            ['label' => 'My pictures', 'url' => Url::to(['user/pictures']),
             'active' => self::getActive($active, UserLayout::pictures())],
        ];
        return $tabs;
    }
}