<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: Dezmont
 * Date: 15.01.2017
 * Time: 19:23
 */
class UploadImageForm extends Model
{
    public $picture = null;


    public function rules() {
        return [
            ['picture', 'required'],
            ['picture', 'file']
        ];
    }




}
