<?php
use yii\db\Migration;

class m170115_173945_create_user_images extends Migration
{

    public $table = 'user_images';


    public function safeUp() {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'type' => $this->string(255)
        ]);
        $this->addForeignKey('fk_user', $this->table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }


    public function safeDown() {
        $this->dropForeignKey('fk_user', $this->table);
        $this->dropTable($this->table);
    }
}
